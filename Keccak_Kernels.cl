/* ============================================================

Copyright (c) 2009-2010 Advanced Micro Devices, Inc.  All rights reserved.
 
Redistribution and use of this material is permitted under the following 
conditions:
 
Redistributions must retain the above copyright notice and all terms of this 
license.
 
In no event shall anyone redistributing or accessing or using this material 
commence or participate in any arbitration or legal action relating to this 
material against Advanced Micro Devices, Inc. or any copyright holders or 
contributors. The foregoing shall survive any expiration or termination of 
this license or any agreement or access or use related to this material. 

ANY BREACH OF ANY TERM OF THIS LICENSE SHALL RESULT IN THE IMMEDIATE REVOCATION 
OF ALL RIGHTS TO REDISTRIBUTE, ACCESS OR USE THIS MATERIAL.

THIS MATERIAL IS PROVIDED BY ADVANCED MICRO DEVICES, INC. AND ANY COPYRIGHT 
HOLDERS AND CONTRIBUTORS "AS IS" IN ITS CURRENT CONDITION AND WITHOUT ANY 
REPRESENTATIONS, GUARANTEE, OR WARRANTY OF ANY KIND OR IN ANY WAY RELATED TO 
SUPPORT, INDEMNITY, ERROR FREE OR UNINTERRUPTED OPERA TION, OR THAT IT IS FREE 
FROM DEFECTS OR VIRUSES.  ALL OBLIGATIONS ARE HEREBY DISCLAIMED - WHETHER 
EXPRESS, IMPLIED, OR STATUTORY - INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED 
WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, 
ACCURACY, COMPLETENESS, OPERABILITY, QUALITY OF SERVICE, OR NON-INFRINGEMENT. 
IN NO EVENT SHALL ADVANCED MICRO DEVICES, INC. OR ANY COPYRIGHT HOLDERS OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, REVENUE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED OR BASED ON ANY THEORY OF LIABILITY 
ARISING IN ANY WAY RELATED TO THIS MATERIAL, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE. THE ENTIRE AND AGGREGATE LIABILITY OF ADVANCED MICRO DEVICES, 
INC. AND ANY COPYRIGHT HOLDERS AND CONTRIBUTORS SHALL NOT EXCEED TEN DOLLARS 
(US $10.00). ANYONE REDISTRIBUTING OR ACCESSING OR USING THIS MATERIAL ACCEPTS 
THIS ALLOCATION OF RISK AND AGREES TO RELEASE ADVANCED MICRO DEVICES, INC. AND 
ANY COPYRIGHT HOLDERS AND CONTRIBUTORS FROM ANY AND ALL LIABILITIES, 
OBLIGATIONS, CLAIMS, OR DEMANDS IN EXCESS OF TEN DOLLARS (US $10.00). THE 
FOREGOING ARE ESSENTIAL TERMS OF THIS LICENSE AND, IF ANY OF THESE TERMS ARE 
CONSTRUED AS UNENFORCEABLE, FAIL IN ESSENTIAL PURPOSE, OR BECOME VOID OR 
DETRIMENTAL TO ADVANCED MICRO DEVICES, INC. OR ANY COPYRIGHT HOLDERS OR 
CONTRIBUTORS FOR ANY REASON, THEN ALL RIGHTS TO REDISTRIBUTE, ACCESS OR USE 
THIS MATERIAL SHALL TERMINATE IMMEDIATELY. MOREOVER, THE FOREGOING SHALL 
SURVIVE ANY EXPIRATION OR TERMINATION OF THIS LICENSE OR ANY AGREEMENT OR 
ACCESS OR USE RELATED TO THIS MATERIAL.

NOTICE IS HEREBY PROVIDED, AND BY REDISTRIBUTING OR ACCESSING OR USING THIS 
MATERIAL SUCH NOTICE IS ACKNOWLEDGED, THAT THIS MATERIAL MAY BE SUBJECT TO 
RESTRICTIONS UNDER THE LAWS AND REGULATIONS OF THE UNITED STATES OR OTHER 
COUNTRIES, WHICH INCLUDE BUT ARE NOT LIMITED TO, U.S. EXPORT CONTROL LAWS SUCH 
AS THE EXPORT ADMINISTRATION REGULATIONS AND NATIONAL SECURITY CONTROLS AS 
DEFINED THEREUNDER, AS WELL AS STATE DEPARTMENT CONTROLS UNDER THE U.S. 
MUNITIONS LIST. THIS MATERIAL MAY NOT BE USED, RELEASED, TRANSFERRED, IMPORTED,
EXPORTED AND/OR RE-EXPORTED IN ANY MANNER PROHIBITED UNDER ANY APPLICABLE LAWS, 
INCLUDING U.S. EXPORT CONTROL LAWS REGARDING SPECIFICALLY DESIGNATED PERSONS, 
COUNTRIES AND NATIONALS OF COUNTRIES SUBJECT TO NATIONAL SECURITY CONTROLS. 
MOREOVER, THE FOREGOING SHALL SURVIVE ANY EXPIRATION OR TERMINATION OF ANY 
LICENSE OR AGREEMENT OR ACCESS OR USE RELATED TO THIS MATERIAL.

NOTICE REGARDING THE U.S. GOVERNMENT AND DOD AGENCIES: This material is 
provided with "RESTRICTED RIGHTS" and/or "LIMITED RIGHTS" as applicable to 
computer software and technical data, respectively. Use, duplication, 
distribution or disclosure by the U.S. Government and/or DOD agencies is 
subject to the full extent of restrictions in all applicable regulations, 
including those found at FAR52.227 and DFARS252.227 et seq. and any successor 
regulations thereof. Use of this material by the U.S. Government and/or DOD 
agencies is acknowledgment of the proprietary rights of any copyright holders 
and contributors, including those of Advanced Micro Devices, Inc., as well as 
the provisions of FAR52.227-14 through 23 regarding privately developed and/or 
commercial computer software.

This license forms the entire agreement regarding the subject matter hereof and 
supersedes all proposals and prior discussions and writings between the parties 
with respect thereto. This license does not affect any ownership, rights, title,
or interest in, or relating to, this material. No terms of this license can be 
modified or waived, and no breach of this license can be excused, unless done 
so in a writing signed by all affected parties. Each term of this license is 
separately enforceable. If any term of this license is determined to be or 
becomes unenforceable or illegal, such term shall be reformed to the minimum 
extent necessary in order for this license to remain in effect in accordance 
with its terms as modified by such reformation. This license shall be governed 
by and construed in accordance with the laws of the State of Texas without 
regard to rules on conflicts of law of any state or jurisdiction or the United 
Nations Convention on the International Sale of Goods. All disputes arising out 
of this license shall be subject to the jurisdiction of the federal and state 
courts in Austin, Texas, and all defenses are hereby waived concerning personal 
jurisdiction and venue of these courts.

============================================================ */

#define ROL64(x, y) (((x) << (y)) | ((x) >> (64 - (y))))

//GPU constants
 __constant ulong KeccakF_RoundConstants[24] =
{
    0x0000000000000001, 0x0000000000008082, 0x800000000000808a,
    0x8000000080008000, 0x000000000000808b, 0x0000000080000001,
    0x8000000080008081, 0x8000000000008009, 0x000000000000008a,
    0x0000000000000088, 0x0000000080008009, 0x000000008000000a,
    0x000000008000808b, 0x800000000000008b, 0x8000000000008089,
    0x8000000000008003, 0x8000000000008002, 0x8000000000000080, 
    0x000000000000800a, 0x800000008000000a, 0x8000000080008081,
    0x8000000000008080, 0x0000000080000001, 0x8000000080008008
};



/* Output tile size : 4x4 = Each thread computes 16 float values*/
/* Required global threads = (widthC / 4, heightC / 4) */
/* This kernel runs on 7xx and CPU as they don't have hardware local memory */
__kernel void mmmKernel(__global uchar *input,
                        __global uchar* output,
            int n/*, int iwidth, int nidx, int r, int c*/)
{
    int pos = (int)(get_global_id(0)); 
    
    ulong state[25];
    
    int iter;
    for(iter = 0; iter < 31; iter++){
        ((uchar*)state)[iter] = input[pos*16+iter];
    }
    for(iter = 32; iter < 200; iter++){
    	((uchar*)state)[iter] = 0;
    }
    ((uchar*)state)[135] ^=  0x80;
    
   
    int nrounds = 24;
    ulong BC[5];
    ulong temp;
    
    int round;
    for(round = 0; round < nrounds; round++){
	int i;
  
		{// Theta
            BC[0] = state[0] ^ state[5] ^ state[10] ^ state[15] ^ state[20];
            BC[1] = state[1] ^ state[6] ^ state[11] ^ state[16] ^ state[21];
            BC[2] = state[2] ^ state[7] ^ state[12] ^ state[17] ^ state[22];
            BC[3] = state[3] ^ state[8] ^ state[13] ^ state[18] ^ state[23];
            BC[4] = state[4] ^ state[9] ^ state[14] ^ state[19] ^ state[24];

            temp = BC[4] ^ ROL64(BC[1], 1);//x=0
            state[0] ^= temp;
            state[5] ^= temp;
            state[10] ^= temp;
            state[15] ^= temp;
            state[20] ^= temp;
            temp = BC[0] ^ ROL64(BC[2], 1);//x=1
            state[1] ^= temp;
            state[6] ^= temp;
            state[11] ^= temp;
            state[16] ^= temp;
            state[21] ^= temp;
            temp = BC[1] ^ ROL64(BC[3], 1);//x=2
            state[2] ^= temp;
            state[7] ^= temp;
            state[12] ^= temp;
            state[17] ^= temp;
            state[22] ^= temp;
            temp = BC[2] ^ ROL64(BC[4], 1);//x=3
            state[3] ^= temp;
            state[8] ^= temp;
            state[13] ^= temp;
            state[18] ^= temp;
            state[23] ^= temp;
            temp = BC[3] ^ ROL64(BC[0], 1);//x=4
            state[4] ^= temp;
            state[9] ^= temp;
            state[14] ^= temp;
            state[19] ^= temp;
            state[24] ^= temp;
        }//end Theta
       
        {
            // Rho Pi
            temp = state[1];
            BC[0] = state[10];
            state[10] = ROL64( temp, 1);
            temp = BC[0];//x=0
            BC[0] =  state[7];
            state[7] = ROL64( temp, 3);
            temp = BC[0];
            BC[0] = state[11];
            state[11] = ROL64( temp, 6);
            temp = BC[0];
            BC[0] = state[17];
            state[17] = ROL64( temp,10);
            temp = BC[0];
            BC[0] = state[18];
            state[18] = ROL64( temp,15);
            temp = BC[0];
            BC[0] =  state[3];
            state[3] = ROL64( temp,21);
            temp = BC[0];//x=5
            BC[0] =  state[5];
            state[5] = ROL64( temp,28);
            temp = BC[0];
            BC[0] = state[16];
            state[16] = ROL64( temp, 36);
            temp = BC[0];
            BC[0] =  state[8];
            state[8] = ROL64( temp,45);
            temp = BC[0];
            BC[0] = state[21];
            state[21] = ROL64( temp,55);
            temp = BC[0];
            BC[0] = state[24];
            state[24] = ROL64( temp, 2);
            temp = BC[0];//x=10
            BC[0] =  state[4];
            state[4] = ROL64( temp,14);
            temp = BC[0];
            BC[0] = state[15];
            state[15] = ROL64( temp,27);
            temp = BC[0];
            BC[0] = state[23];
            state[23] = ROL64( temp, 41);
            temp = BC[0];
            BC[0] = state[19];
            state[19] = ROL64( temp,56);
            temp = BC[0];
            BC[0] = state[13];
            state[13] = ROL64( temp, 8);
            temp = BC[0];//x=15
            BC[0] = state[12];
            state[12] = ROL64( temp,25);
            temp = BC[0];
            BC[0] =  state[2];
            state[2] = ROL64( temp,43);
            temp = BC[0];
            BC[0] = state[20];
            state[20] = ROL64( temp,62);
            temp = BC[0];
            BC[0] = state[14];
            state[14] = ROL64( temp,18);
            temp = BC[0];
            BC[0] = state[22];
            state[22] = ROL64( temp, 39);
            temp = BC[0];//x=20
            BC[0] =  state[9];
            state[9] = ROL64( temp,61);
            temp = BC[0];
            BC[0] =  state[6];
            state[6] = ROL64( temp,20);
            temp = BC[0];
            BC[0] =  state[1];
            state[1] = ROL64( temp,44);
            temp = BC[0];//x=23
        }//end Rho Pi

        {
            //	Chi
            BC[0] = state[0];
            BC[1] = state[1];
            BC[2] = state[2];
            BC[3] = state[3];
            BC[4] = state[4];
            state[0] = BC[0] ^((~BC[1]) & BC[2]);
            state[1] = BC[1] ^((~BC[2]) & BC[3]);
            state[2] = BC[2] ^((~BC[3]) & BC[4]);
            state[3] = BC[3] ^((~BC[4]) & BC[0]);
            state[4] = BC[4] ^((~BC[0]) & BC[1]);
            BC[0] = state[5];
            BC[1] = state[6];
            BC[2] = state[7];
            BC[3] = state[8];
            BC[4] = state[9];
            state[5] = BC[0] ^((~BC[1]) & BC[2]);
            state[6] = BC[1] ^((~BC[2]) & BC[3]);
            state[7] = BC[2] ^((~BC[3]) & BC[4]);
            state[8] = BC[3] ^((~BC[4]) & BC[0]);
            state[9] = BC[4] ^((~BC[0]) & BC[1]);
            BC[0] = state[10];
            BC[1] = state[11];
            BC[2] = state[12];
            BC[3] = state[13];
            BC[4] = state[14];
            state[10] = BC[0] ^((~BC[1]) & BC[2]);
            state[11] = BC[1] ^((~BC[2]) & BC[3]);
            state[12] = BC[2] ^((~BC[3]) & BC[4]);
            state[13] = BC[3] ^((~BC[4]) & BC[0]);
            state[14] = BC[4] ^((~BC[0]) & BC[1]);
            BC[0] = state[15];
            BC[1] = state[16];
            BC[2] = state[17];
            BC[3] = state[18];
            BC[4] = state[19];
            state[15] = BC[0] ^((~BC[1]) & BC[2]);
            state[16] = BC[1] ^((~BC[2]) & BC[3]);
            state[17] = BC[2] ^((~BC[3]) & BC[4]);
            state[18] = BC[3] ^((~BC[4]) & BC[0]);
            state[19] = BC[4] ^((~BC[0]) & BC[1]);
            BC[0] = state[20];
            BC[1] = state[21];
            BC[2] = state[22];
            BC[3] = state[23];
            BC[4] = state[24];
            state[20] = BC[0] ^((~BC[1]) & BC[2]);
            state[21] = BC[1] ^((~BC[2]) & BC[3]);
            state[22] = BC[2] ^((~BC[3]) & BC[4]);
            state[23] = BC[3] ^((~BC[4]) & BC[0]);
            state[24] = BC[4] ^((~BC[0]) & BC[1]);
        }//end Chi

        //	Iota
        state[0] ^= KeccakF_RoundConstants[round];
        
    }
    for(iter = 0; iter < 32; iter++){
    	output[pos*n + iter] = ((uchar*)state)[iter];
    }
}

