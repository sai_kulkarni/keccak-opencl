/* ============================================================

Copyright (c) 2009 Advanced Micro Devices, Inc.  All rights reserved.
 
Redistribution and use of this material is permitted under the following 
conditions:
 
Redistributions must retain the above copyright notice and all terms of this 
license.
 
In no event shall anyone redistributing or accessing or using this material 
commence or participate in any arbitration or legal action relating to this 
material against Advanced Micro Devices, Inc. or any copyright holders or 
contributors. The foregoing shall survive any expiration or termination of 
this license or any agreement or access or use related to this material. 

ANY BREACH OF ANY TERM OF THIS LICENSE SHALL RESULT IN THE IMMEDIATE REVOCATION 
OF ALL RIGHTS TO REDISTRIBUTE, ACCESS OR USE THIS MATERIAL.

THIS MATERIAL IS PROVIDED BY ADVANCED MICRO DEVICES, INC. AND ANY COPYRIGHT 
HOLDERS AND CONTRIBUTORS "AS IS" IN ITS CURRENT CONDITION AND WITHOUT ANY 
REPRESENTATIONS, GUARANTEE, OR WARRANTY OF ANY KIND OR IN ANY WAY RELATED TO 
SUPPORT, INDEMNITY, ERROR FREE OR UNINTERRUPTED OPERA TION, OR THAT IT IS FREE 
FROM DEFECTS OR VIRUSES.  ALL OBLIGATIONS ARE HEREBY DISCLAIMED - WHETHER 
EXPRESS, IMPLIED, OR STATUTORY - INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED 
WARRANTIES OF TITLE, MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, 
ACCURACY, COMPLETENESS, OPERABILITY, QUALITY OF SERVICE, OR NON-INFRINGEMENT. 
IN NO EVENT SHALL ADVANCED MICRO DEVICES, INC. OR ANY COPYRIGHT HOLDERS OR 
CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE,
EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, REVENUE, DATA, OR PROFITS; OR 
BUSINESS INTERRUPTION) HOWEVER CAUSED OR BASED ON ANY THEORY OF LIABILITY 
ARISING IN ANY WAY RELATED TO THIS MATERIAL, EVEN IF ADVISED OF THE POSSIBILITY 
OF SUCH DAMAGE. THE ENTIRE AND AGGREGATE LIABILITY OF ADVANCED MICRO DEVICES, 
INC. AND ANY COPYRIGHT HOLDERS AND CONTRIBUTORS SHALL NOT EXCEED TEN DOLLARS 
(US $10.00). ANYONE REDISTRIBUTING OR ACCESSING OR USING THIS MATERIAL ACCEPTS 
THIS ALLOCATION OF RISK AND AGREES TO RELEASE ADVANCED MICRO DEVICES, INC. AND 
ANY COPYRIGHT HOLDERS AND CONTRIBUTORS FROM ANY AND ALL LIABILITIES, 
OBLIGATIONS, CLAIMS, OR DEMANDS IN EXCESS OF TEN DOLLARS (US $10.00). THE 
FOREGOING ARE ESSENTIAL TERMS OF THIS LICENSE AND, IF ANY OF THESE TERMS ARE 
CONSTRUED AS UNENFORCEABLE, FAIL IN ESSENTIAL PURPOSE, OR BECOME VOID OR 
DETRIMENTAL TO ADVANCED MICRO DEVICES, INC. OR ANY COPYRIGHT HOLDERS OR 
CONTRIBUTORS FOR ANY REASON, THEN ALL RIGHTS TO REDISTRIBUTE, ACCESS OR USE 
THIS MATERIAL SHALL TERMINATE IMMEDIATELY. MOREOVER, THE FOREGOING SHALL 
SURVIVE ANY EXPIRATION OR TERMINATION OF THIS LICENSE OR ANY AGREEMENT OR 
ACCESS OR USE RELATED TO THIS MATERIAL.

NOTICE IS HEREBY PROVIDED, AND BY REDISTRIBUTING OR ACCESSING OR USING THIS 
MATERIAL SUCH NOTICE IS ACKNOWLEDGED, THAT THIS MATERIAL MAY BE SUBJECT TO 
RESTRICTIONS UNDER THE LAWS AND REGULATIONS OF THE UNITED STATES OR OTHER 
COUNTRIES, WHICH INCLUDE BUT ARE NOT LIMITED TO, U.S. EXPORT CONTROL LAWS SUCH 
AS THE EXPORT ADMINISTRATION REGULATIONS AND NATIONAL SECURITY CONTROLS AS 
DEFINED THEREUNDER, AS WELL AS STATE DEPARTMENT CONTROLS UNDER THE U.S. 
MUNITIONS LIST. THIS MATERIAL MAY NOT BE USED, RELEASED, TRANSFERRED, IMPORTED,
EXPORTED AND/OR RE-EXPORTED IN ANY MANNER PROHIBITED UNDER ANY APPLICABLE LAWS, 
INCLUDING U.S. EXPORT CONTROL LAWS REGARDING SPECIFICALLY DESIGNATED PERSONS, 
COUNTRIES AND NATIONALS OF COUNTRIES SUBJECT TO NATIONAL SECURITY CONTROLS. 
MOREOVER, THE FOREGOING SHALL SURVIVE ANY EXPIRATION OR TERMINATION OF ANY 
LICENSE OR AGREEMENT OR ACCESS OR USE RELATED TO THIS MATERIAL.

NOTICE REGARDING THE U.S. GOVERNMENT AND DOD AGENCIES: This material is 
provided with "RESTRICTED RIGHTS" and/or "LIMITED RIGHTS" as applicable to 
computer software and technical data, respectively. Use, duplication, 
distribution or disclosure by the U.S. Government and/or DOD agencies is 
subject to the full extent of restrictions in all applicable regulations, 
including those found at FAR52.227 and DFARS252.227 et seq. and any successor 
regulations thereof. Use of this material by the U.S. Government and/or DOD 
agencies is acknowledgment of the proprietary rights of any copyright holders 
and contributors, including those of Advanced Micro Devices, Inc., as well as 
the provisions of FAR52.227-14 through 23 regarding privately developed and/or 
commercial computer software.

This license forms the entire agreement regarding the subject matter hereof and 
supersedes all proposals and prior discussions and writings between the parties 
with respect thereto. This license does not affect any ownership, rights, title,
or interest in, or relating to, this material. No terms of this license can be 
modified or waived, and no breach of this license can be excused, unless done 
so in a writing signed by all affected parties. Each term of this license is 
separately enforceable. If any term of this license is determined to be or 
becomes unenforceable or illegal, such term shall be reformed to the minimum 
extent necessary in order for this license to remain in effect in accordance 
with its terms as modified by such reformation. This license shall be governed 
by and construed in accordance with the laws of the State of Texas without 
regard to rules on conflicts of law of any state or jurisdiction or the United 
Nations Convention on the International Sale of Goods. All disputes arising out 
of this license shall be subject to the jurisdiction of the federal and state 
courts in Austin, Texas, and all defenses are hereby waived concerning personal 
jurisdiction and venue of these courts.

============================================================ */

#include<stdio.h>
#include "Keccak.hpp"
#include <iostream>
#include <fstream>
#include <bitset>
using namespace std;

int
Keccak::setupKeccak()
{
	// allocate and init memory used by host  hinput[iwidth]
	cl_uint inputSizeBytes = (iwidth) * sizeof(cl_uchar);

	hinput = (cl_uchar *) malloc(inputSizeBytes);
	CHECK_ALLOCATION(hinput, "Failed to allocate host memory. (hinput)");
	memset(hinput, 0, inputSizeBytes);

	ifstream fp;
	fp.open("dictionary.txt");
	cl_int countedwords = 0;

	char nextstr[20];

	do{
		fp.getline(nextstr ,maxlen);
		cl_int size = strlen(nextstr);
	    if(size!=0){
	    	strncpy((char*)&hinput[countedwords*(maxlen+1)], nextstr, size);
	    	hinput[countedwords*(maxlen+1)+size] = 0x01;
	    	countedwords++;
		}
	}while((countedwords < nin) && !fp.eof());
	cout << "countedwords " << countedwords << " nin " << nin << endl;
	fp.close();


    for(int i = 0; i < 80; i++){
    	cout << (bitset<8>) hinput[i] << " ";
    	if((i+1)%(maxlen+1) == 0) cout << endl;
    }

    // allocate memory for output[width1][height0]
    cl_uint outputSizeBytes = owidth * sizeof(cl_uchar);

    houtput = (cl_uchar *) malloc(outputSizeBytes);
    CHECK_ALLOCATION(houtput, "Failed to allocate host memory. (houtput)");

    // allocate memory for output[width1][height0] of reference implemenation
    if(verify)
    {
        refoutput = (cl_uchar *) malloc(outputSizeBytes);
        CHECK_ALLOCATION(refoutput, "Failed to allocate host memory. (verificationOutput)");
        memset(refoutput, 0, outputSizeBytes);
    }
    
    // Unless quiet mode has been enabled, print the INPUT arrays

    if(!quiet){
        sampleCommon->printArray<cl_uchar>(
            "Input",
            hinput,
            iwidth,
            1);
    }

    return SDK_SUCCESS;
}

/**
 * genBinary Image function is used to when we want to create the binary 
 * for the kernel and run it at a later time. This is useful where offline  
 * compilation is the preferred mechanism. 
 */
int 
Keccak::genBinaryImage()
{
    streamsdk::bifData binaryData;
    binaryData.kernelName = std::string("Keccak_Kernels.cl");
    binaryData.flagsStr = std::string("");
    if(isComplierFlagsSpecified())
        binaryData.flagsFileName = std::string(flags.c_str());

    binaryData.binaryName = std::string(dumpBinary.c_str());
    int status = sampleCommon->generateBinaryImage(binaryData);
    return status;
}



int
Keccak::setupCL(void)
{
    cl_int status = 0;
    cl_device_type dType;
    
    if( deviceType.compare("cpu") == 0)
    {
        dType = CL_DEVICE_TYPE_CPU;
    }
    else //deviceType = "gpu" 
    {
        dType = CL_DEVICE_TYPE_GPU;
        if(selectCPU || isThereGPU() == false)
        {
            std::cout << "GPU not found. Falling back to CPU device" << std::endl;
            dType = CL_DEVICE_TYPE_CPU;
        }
    }


    /*
     * Have a look at the available platforms and pick either
     * the AMD one if available or a reasonable default.
     */
    cl_platform_id platform = NULL;
    int retValue = sampleCommon->getPlatform(platform, platformId, isPlatformEnabled());
    CHECK_ERROR(retValue, SDK_SUCCESS, "sampleCommon::getPlatform() failed");
    
    // Display available devices.
    retValue = sampleCommon->displayDevices(platform, dType);
    CHECK_ERROR(retValue, SDK_SUCCESS, "sampleCommon::displayDevices() failed");

    printf("creating context\n");
    // creating context
    cl_context_properties cps[3] = 
    {
        CL_CONTEXT_PLATFORM, 
        (cl_context_properties)platform, 
        0
    };

    context = clCreateContextFromType(
                  cps,
                  dType,
                  NULL,
                  NULL,
                  &status);
    CHECK_OPENCL_ERROR(status, "clCreateContextFromType failed.");

    // getting device on which to run the sample
    status = sampleCommon->getDevices(context, &devices, deviceId, isDeviceIdEnabled());
    CHECK_ERROR(status, SDK_SUCCESS, "sampleCommon::getDevices() failed");
 
    //Set device info of given cl_device_id
    retValue = deviceInfo.setDeviceInfo(devices[deviceId]);
    CHECK_ERROR(retValue, SDK_SUCCESS, "SDKDeviceInfo::setDeviceInfo() failed");

    printf("context created\n");


    printf("creating command queue\n");
    {
        // The block is to move the declaration of prop closer to its use 
        cl_command_queue_properties prop = 0;
        if(!eAppGFLOPS)
            prop |= CL_QUEUE_PROFILING_ENABLE;

        commandQueue = clCreateCommandQueue(
                           context, 
                           devices[deviceId], 
                           prop, 
                           &status);
        CHECK_OPENCL_ERROR(status, "clCreateCommandQueue failed.");
    }
    printf("command queue created\n");

    // Set Presistent memory only for AMD platform
    cl_mem_flags inMemFlags = CL_MEM_READ_ONLY;
    if(isAmdPlatform())
        inMemFlags |= CL_MEM_USE_PERSISTENT_MEM_AMD;

    // Create buffer for hash input
    inputBuffer = clCreateBuffer(
                      context, 
                      inMemFlags,
                      (sizeof(cl_uchar) * iwidth),
                      0, 
                      &status);
    CHECK_OPENCL_ERROR(status, "clCreateBuffer failed. (inputBuffer)");

    outputBuffer = clCreateBuffer(
                      context, 
                      CL_MEM_WRITE_ONLY | CL_MEM_ALLOC_HOST_PTR,
                      (sizeof(cl_uchar) * owidth),
                      0, 
                      &status);
    CHECK_OPENCL_ERROR(status, "clCreateBuffer failed. (outputBuffer)");

    printf("in out buffers created\n");

    // create a CL program using the kernel source 
    streamsdk::buildProgramData buildData;
    buildData.kernelName = std::string("Keccak_Kernels.cl");
    buildData.devices = devices;
    buildData.deviceId = deviceId;
    buildData.flagsStr = std::string("");
    if(isLoadBinaryEnabled())
        buildData.binaryName = std::string(loadBinary.c_str());

    if(isComplierFlagsSpecified())
        buildData.flagsFileName = std::string(flags.c_str());

    retValue = sampleCommon->buildOpenCLProgram(program, context, buildData);
    CHECK_ERROR(retValue, SDK_SUCCESS, "sampleCommon::buildOpenCLProgram() failed");
    
    printf("kernel built\n");

    // If local memory is present then use the specific kernel 
    if(lds)
        kernel = clCreateKernel(program, "mmmKernel_local", &status);
    else
        kernel = clCreateKernel(program, "mmmKernel", &status);
    CHECK_OPENCL_ERROR(status, "clCreateKernel failed.");

    printf("kernel created\n");

    return SDK_SUCCESS;
}

int
Keccak::setWorkGroupSize()
{
    /* 
     * Kernel runs over complete output matrix with blocks of blockSize x blockSize 
     * running concurrently
     */
    cl_int status = 0;

    globalThreads = nin;
    localThreads = deviceInfo.maxWorkGroupSize;
    // Setting the KernelWorkGroupInfo values
    status = kernelInfo.setKernelWorkGroupInfo(kernel, devices[deviceId]);
    CHECK_ERROR(status,0, "setKernelWrkGroupInfo failed");
    
    availableLocalMemory = deviceInfo.localMemSize - kernelInfo.localMemoryUsed;
    neededLocalMemory    = 200 * sizeof(cl_uchar);
    if(neededLocalMemory > availableLocalMemory)
    {
        std::cout << "Unsupported: Insufficient local memory on device." << std::endl;
        return SDK_SUCCESS;
    }

    if((cl_uint)localThreads  > kernelInfo.kernelWorkGroupSize)
    {
       if(kernelInfo.kernelWorkGroupSize >= 64)
        {
            localThreads = 64;
        }
        else if(kernelInfo.kernelWorkGroupSize >= 32)
        {
            localThreads = 32;
        }
        else
        {
            std::cout << "Out of Resources!" << std::endl;
            std::cout << "Group Size specified : " << localThreads << std::endl;
            std::cout << "Max Group Size supported on the kernel : "
                      << kernelInfo.kernelWorkGroupSize<<std::endl;
            return SDK_FAILURE;
        }
    }

    if(localThreads > deviceInfo.maxWorkItemSizes[0])
    {
        std::cout << "Unsupported: Device does not support requested number of work items." << std::endl;
        return SDK_FAILURE;
    }

    return SDK_SUCCESS;
}


int 
Keccak::runCLKernels(void)
{
    cl_int   status;
    status = setWorkGroupSize();
    CHECK_ERROR(status, SDK_SUCCESS, "getWorkGroupSize() failed");
    cout << "work group size set" << endl;
    cl_event ndrEvt;
    cl_int eventStatus = CL_QUEUED;

    // Set input data to input buffer
    cl_event inMapEvt, inUnmapEvt, outMapEvt, outUnmapEvt;
    void* mapPtr = clEnqueueMapBuffer(
                        commandQueue, 
                        inputBuffer,
                        CL_FALSE, 
                        CL_MAP_WRITE, 
                        0, 
                        iwidth * sizeof(cl_uchar),
                        0, 
                        NULL, 
                        &inMapEvt,
                        &status);
    CHECK_OPENCL_ERROR(status, "clEnqueueMapBuffer failed. (inputBuffer)");

    status = clFlush(commandQueue);
	CHECK_OPENCL_ERROR(status, "clFlush failed.");

	status = sampleCommon->waitForEventAndRelease(&inMapEvt);
	CHECK_ERROR(status,SDK_SUCCESS, "WaitForEventAndRelease(inMapEvt) Failed");
	memcpy(mapPtr, hinput, sizeof(cl_uchar) * iwidth);

    cout << "input copied" << endl;

	status = clEnqueueUnmapMemObject(
				commandQueue,
				inputBuffer,
				mapPtr,
				0,
				NULL,
				&inUnmapEvt);
	CHECK_OPENCL_ERROR(status, "clEnqueueUnmapMemObject failed. (inputBuffer)");

	status = clFlush(commandQueue);
	CHECK_OPENCL_ERROR(status, "clFlush failed.");

	status = sampleCommon->waitForEventAndRelease(&inUnmapEvt);
	CHECK_ERROR(status, SDK_SUCCESS, "waitForEventAndRelease(inUnmapEvt) failed");

	cout << "input unmapped" << endl;

    // Set appropriate arguments to the kernel

    // 1st argument input array
    status = clSetKernelArg(
                    kernel, 
                    0, 
                    sizeof(cl_mem), 
                    (void *)&inputBuffer);
    CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (inputBuffer)");

    // 3rd argument - output buffer
    status = clSetKernelArg(
                    kernel, 
                    1,
                    sizeof(cl_mem), 
                    (void *)&outputBuffer);
    CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (outputBuffer)");

	//6th argument - n: hash size
		status = clSetKernelArg(
					kernel,
					2,
					sizeof(cl_int),
					(void*)&n);
	CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (n)");

/*
    // 4th argument - iwidth
    status = clSetKernelArg(
                    kernel, 
                    3,
                    sizeof(cl_int),
                    (void*)&iwidth);
    CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (iwidth)");

    //5th argument - nidx
	status = clSetKernelArg(
					kernel,
					4,
					sizeof(cl_int),
					(void*)&nin);
	CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (nidx)");


	//7th argument - r: rate
			status = clSetKernelArg(
					kernel,
					5,
					sizeof(cl_int),
					(void*)&r);
	CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (r)");

	//7th argument - c: capacity
			status = clSetKernelArg(
					kernel,
					6,
					sizeof(cl_int),
					(void*)&c);
	CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (c)");

	*/

	cout << "arguments set" << endl;
    /*if(lds)
    {
    	status = clSetKernelArg(
                        kernel, 
                        4, 
                         * sizeof(cl_float),
                        NULL);
        CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (local memory)");
    }
    else
    {
        status = clSetKernelArg(kernel, 4, sizeof(cl_int), &width1);
        CHECK_OPENCL_ERROR(status, "clSetKernelArg failed. (width1)");
    }*/

    // Enqueue a kernel run call
    status = clEnqueueNDRangeKernel(
                 commandQueue,
                 kernel,
                 1,
                 NULL,
                 &globalThreads,
                 &localThreads,
                 0,
                 NULL,
                 &ndrEvt);    
    CHECK_OPENCL_ERROR(status, "clEnqueueNDRangeKernel failed.");

    status = clFlush(commandQueue);
    CHECK_OPENCL_ERROR(status, "clFlush failed.");

    cout << "kernel launched" << endl;

    // wait for the kernel call to finish execution
    eventStatus = CL_QUEUED;
    while(eventStatus != CL_COMPLETE)
    {
        status = clGetEventInfo(
                        ndrEvt, 
                        CL_EVENT_COMMAND_EXECUTION_STATUS, 
                        sizeof(cl_int),
                        &eventStatus,
                        NULL);
        CHECK_OPENCL_ERROR(status, "clGetEventInfo failed.");
    }
        // Calculate performance
        cl_ulong startTime;
        cl_ulong endTime;
        
        // Get kernel profiling info
        status = clGetEventProfilingInfo(ndrEvt,
                                         CL_PROFILING_COMMAND_START,
                                         sizeof(cl_ulong),
                                         &startTime,
                                         0);
        CHECK_OPENCL_ERROR(status, "clGetEventProfilingInfo failed.(startTime)");

        status = clGetEventProfilingInfo(ndrEvt,
                                         CL_PROFILING_COMMAND_END,
                                         sizeof(cl_ulong),
                                         &endTime,
                                         0);
        CHECK_OPENCL_ERROR(status, "clGetEventProfilingInfo failed.(endTime)");

        // Print performance numbers
        double sec = 1e-9 * (endTime - startTime);
        kernelTime += sec;


    status = clReleaseEvent(ndrEvt);
    CHECK_OPENCL_ERROR(status, "clReleaseEvent failed. (ndrEvt)");

    cout << "kernel event released" << endl;

    void* outMapPtr = clEnqueueMapBuffer(
                        commandQueue, 
                        outputBuffer, 
                        CL_FALSE, 
                        CL_MAP_READ, 
                        0, 
                        owidth * sizeof(cl_uchar),
                        0, 
                        NULL, 
                        &outMapEvt, 
                        &status);
    CHECK_OPENCL_ERROR(status, "clEnqueueMapBuffer failed. (outputBuffer)");

    status = clFlush(commandQueue);
    CHECK_OPENCL_ERROR(status, "clFlush failed.");

    status = sampleCommon->waitForEventAndRelease(&outMapEvt);
    CHECK_ERROR(status,0, "waitForEventAndRelease(outMapEvt) failed");
    memcpy(houtput, outMapPtr, sizeof(cl_uchar) * owidth);

    cout << "output copied " << endl;

    for(int i = 0 ; i < 160; i++){
    	cout << hex <<(int) houtput[i] << " ";
    	if((i+1)%n == 0){
    		cout << endl;
    	}
    }
    status = clEnqueueUnmapMemObject(
                commandQueue, 
                outputBuffer, 
                outMapPtr, 
                0, 
                NULL, 
                &outUnmapEvt);
    CHECK_OPENCL_ERROR(status, "clEnqueueUnmapMemObject failed. (outputBuffer)");

    status = clFlush(commandQueue);
    CHECK_OPENCL_ERROR(status, "clFlush failed.");

    status = sampleCommon->waitForEventAndRelease(&outUnmapEvt);
    CHECK_ERROR(status,0, "waitForEventAndRelease(outUnmapEvt) failed");

    return SDK_SUCCESS;
}


/*
 * This is a naive O(N^3) CPU implementation of matrix multiplication
 */
void keccakCPUReference(
   cl_uchar * houtput,
   cl_uchar * hinput,
   const cl_uint hiwidth,
   const cl_uint howidth){

	return;
}


int
Keccak::initialize()
{
    // Call base class Initialize to get default configuration
    if(this->SDKSample::initialize() != SDK_SUCCESS)
        return SDK_FAILURE;

    // add an option for getting blockSize from commandline
    streamsdk::Option* kParam = new streamsdk::Option;
    CHECK_ALLOCATION(kParam, "Memory Allocation error.\n");
    kParam->_sVersion = "k";
    kParam->_lVersion = "numthreads";
    kParam->_description = "Number of dictionary words";
    kParam->_type     = streamsdk::CA_ARG_INT;
    kParam->_value    = &k;
    sampleArgs->AddOption(kParam);
    delete kParam;

    streamsdk::Option* iParam = new streamsdk::Option;
    CHECK_ALLOCATION(iParam, "Memory Allocation error.\n");
    iParam->_sVersion = "p";
    iParam->_lVersion = "password";
    iParam->_description = "the password being attacked";
    iParam->_type     = streamsdk::CA_ARG_STRING;
    iParam->_value    = &instr;
    sampleArgs->AddOption(iParam);
    delete iParam;

    streamsdk::Option* cpu_option = new streamsdk::Option;
    CHECK_ALLOCATION(cpu_option, "Memory Allocation error.\n");
    cpu_option->_sVersion = "c";
    cpu_option->_lVersion = "cpu";
    cpu_option->_description = "Selects the kernel to run on CPU";
    cpu_option->_type = streamsdk::CA_NO_ARGUMENT;
    cpu_option->_value = &selectCPU;
    sampleArgs->AddOption(cpu_option);
    delete cpu_option;

/*    streamsdk::Option* zParam = new streamsdk::Option;
    CHECK_ALLOCATION(zParam, "Memory Allocation error.\n");
    zParam->_sVersion = "z";
    zParam->_lVersion = "width1";
    zParam->_description = "width of matrix B";
    zParam->_type     = streamsdk::CA_ARG_INT;
    zParam->_value    = &k;
    sampleArgs->AddOption(zParam);
    delete zParam;

    streamsdk::Option* blockSizeParam = new streamsdk::Option;
    CHECK_ALLOCATION(blockSizeParam, "Memory Allocation error.\n");
    blockSizeParam->_sVersion = "b";
    blockSizeParam->_lVersion = "blockSize";
    blockSizeParam->_description = "Use local memory of dimensions blockSize x blockSize";
    blockSizeParam->_type     = streamsdk::CA_ARG_INT;
    blockSizeParam->_value    = &blockSize;
    sampleArgs->AddOption(blockSizeParam);
    delete blockSizeParam;

    streamsdk::Option* num_iterations = new streamsdk::Option;
    CHECK_ALLOCATION(num_iterations, "Memory Allocation error.\n");
    num_iterations->_sVersion = "i";
    num_iterations->_lVersion = "iterations";
    num_iterations->_description = "Number of iterations for kernel execution";
    num_iterations->_type = streamsdk::CA_ARG_INT;
    num_iterations->_value = &iterations;
    sampleArgs->AddOption(num_iterations);
    delete num_iterations;
*/
    streamsdk::Option* appGflops_option = new streamsdk::Option;
    CHECK_ALLOCATION(appGflops_option, "Memory Allocation error.\n");
    appGflops_option->_sVersion = "";
    appGflops_option->_lVersion = "eAppGflops";
    appGflops_option->_description = "Prints GFLOPS calculated from transfer + kernel time";
    appGflops_option->_type = streamsdk::CA_NO_ARGUMENT;
    appGflops_option->_value = &eAppGFLOPS;
    sampleArgs->AddOption(appGflops_option);
    delete appGflops_option;

    return SDK_SUCCESS;
}

int 
Keccak::setup()
{  
//	iterations = 2;
	quiet = true;
    nin  = k;
    owidth = nin*n;
    iwidth = nin*(maxlen+1);
    cout << "nidx " << nin << " owidth " << owidth << " iwidth " << iwidth << endl;
    pass = (cl_uchar*) instr.c_str();
    cout << "password = " << instr << k << endl;

    if(setupKeccak()!=SDK_SUCCESS)
        return SDK_FAILURE;

    int timer = sampleCommon->createTimer();
    sampleCommon->resetTimer(timer);
    sampleCommon->startTimer(timer);

    if(setupCL()!=SDK_SUCCESS)
        return SDK_FAILURE;

    sampleCommon->stopTimer(timer);

    setupTime = (cl_double)sampleCommon->readTimer(timer);

    return SDK_SUCCESS;
}


int 
Keccak::run()
{
    // Warm up
	cout << "in keccak::run" << endl;
    for(int i = 0; i < 2 && iterations != 1; i++)
    {
        cout << "here" << endl;
        // Arguments are set and execution call is enqueued on command buffer
        if(runCLKernels() != SDK_SUCCESS)
            return SDK_FAILURE;
    }
    cout << "warmed up" << endl;
    int timer = sampleCommon->createTimer();
    sampleCommon->resetTimer(timer);
    sampleCommon->startTimer(timer);

    std::cout << "Executing kernel for " << iterations << " iterations" << std::endl;
    std::cout << "-------------------------------------------" << std::endl;

    kernelTime = 0;
    for(int i = 0; i < iterations; i++)
    {
        // Arguments are set and execution call is enqueued on command buffer
        int kernelRun = runCLKernels();
        if(kernelRun != SDK_SUCCESS)
        {
            return kernelRun;
        }
    }

    sampleCommon->stopTimer(timer);
    appTime = (double)(sampleCommon->readTimer(timer)) / iterations;
    kernelTime = kernelTime / iterations;

    if(!quiet) {
        sampleCommon->printArray<cl_uchar>("Output", houtput, owidth, 1);
    }

    return SDK_SUCCESS;
}

int 
Keccak::verifyResults()
{
    /*if(verify)
    {
        // reference implementation
        matrixMultiplicationCPUReference(verificationOutput, input0, input1, height0, width0,  width1);

        // compare the results and see if they match
        if(sampleCommon->compare(output, verificationOutput, height0*width1))
        {
            std::cout<<"Passed!\n" << std::endl;
            return SDK_SUCCESS;
        }
        else
        {
            std::cout<<"Failed\n" << std::endl;
            return SDK_FAILURE;
        }
    }
*/
    return SDK_SUCCESS;
}

void 
Keccak::printStats()
{
        std::string strArray[3] = {"NumInputs", "Time(sec)", "kernelTime(sec)"};
        std::string stats[3];

        totalTime = setupTime + kernelTime;

        stats[0]  = sampleCommon->toString(nin, std::dec);
        stats[1]  = sampleCommon->toString(totalTime, std::dec);
        stats[2]  = sampleCommon->toString(kernelTime, std::dec);
        this->SDKSample::printStats(strArray, stats, 3);
}

int 
Keccak::cleanup()
{
    // Releases OpenCL resources (Context, Memory etc.)
    cl_int status;

    status = clReleaseKernel(kernel);
    CHECK_OPENCL_ERROR(status, "clReleaseKernel failed.(kernel)");

    printf("kernel released\n");
    status = clReleaseProgram(program);
    CHECK_OPENCL_ERROR(status, "clReleaseProgram failed.(program)");

    printf("program released\n");
    status = clReleaseMemObject(inputBuffer);
    CHECK_OPENCL_ERROR(status, "clReleaseMemObject failed.(inputBuffer0)");

    printf("inputbuffer released\n");
    status = clReleaseMemObject(outputBuffer);
    CHECK_OPENCL_ERROR(status, "clReleaseMemObject failed.(outputBuffer)");

    printf("outputbuffer released\n");
    status = clReleaseCommandQueue(commandQueue);
    CHECK_OPENCL_ERROR(status, "clReleaseCommandQueue failed.(commandQueue)");

    printf("command queue released\n");
    status = clReleaseContext(context);
    CHECK_OPENCL_ERROR(status, "clReleaseContext failed.(context)");

    printf("context released\n");
    // release program resources (input memory etc.)
    FREE(hinput);
    printf("hinput freed\n");
    FREE(houtput);
    printf("houtput freed\n");
    //FREE(verificationOutput);
    FREE(devices);
    printf("devices freed\n");
    return SDK_SUCCESS;
}

int 
main(int argc, char * argv[])
{
    Keccak clKeccak("OpenCL Keccak");

    printf("created class\n");

    if(clKeccak.initialize() != SDK_SUCCESS)
        return SDK_FAILURE;
    printf("initialize done\n");

    if(clKeccak.parseCommandLine(argc, argv) != SDK_SUCCESS)
        return SDK_FAILURE;

    printf("parse commands done\n");

    if(clKeccak.isDumpBinaryEnabled() != SDK_SUCCESS)
    {
        return clKeccak.genBinaryImage();
    }
    else
    {
        // Setup
        if(clKeccak.setup() != SDK_SUCCESS)
            return SDK_FAILURE;

        printf("setup done\n");
        // Run
        if(clKeccak.run() != SDK_SUCCESS)
            return SDK_FAILURE;
        printf("run done\n");
        // VerifyResults
        /*if(clKeccak.verifyResults() != SDK_SUCCESS)
            return SDK_FAILURE;
         */
        // Cleanup
        if(clKeccak.cleanup() != SDK_SUCCESS)
            return SDK_FAILURE;
        printf("cleanup done\n");
        printf("printing stats!\n");
        clKeccak.printStats();
    }

    return SDK_SUCCESS;
}
